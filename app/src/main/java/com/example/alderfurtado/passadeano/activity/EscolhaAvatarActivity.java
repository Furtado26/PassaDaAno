package com.example.alderfurtado.passadeano.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Avatar;
import com.example.alderfurtado.passadeano.models.Jogador;

import io.realm.Realm;

public class EscolhaAvatarActivity extends AppCompatActivity implements View.OnClickListener{

    Realm realm = Realm.getDefaultInstance();
    CardView btn_programador;
    CardView btn_suporte;
    CardView btn_design;
    CardView btn_asiatico;
    Jogador jogador;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolha_avatar);

        id = getIntent().getStringExtra("id");
        jogador = realm.where(Jogador.class).equalTo("id",id).findFirst();

        btn_programador = findViewById(R.id.btn_programador);
        btn_design = findViewById(R.id.btn_design);
        btn_suporte = findViewById(R.id.btn_suporte);
        btn_asiatico = findViewById(R.id.btn_asiatico);

        btn_programador.setOnClickListener(this);
        btn_design.setOnClickListener(this);
        btn_suporte.setOnClickListener(this);
        btn_asiatico.setOnClickListener(this);

    }

    public void mensagem(String msg){
        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getBaseContext(),NivelamentoActivity.class);
        realm.beginTransaction();
        Avatar avatar = realm.createObject(Avatar.class);

        switch(view.getId()){

            case R.id.btn_programador:

                avatar.setNome("programador");
                avatar.setCategorias("programador");
                jogador.setAvatar(avatar);
                realm.commitTransaction();
                intent.putExtra("id",id);
                startActivity(intent);
                break;

            case R.id.btn_design:
                avatar.setNome("design");
                avatar.setCategorias("design");
                jogador.setAvatar(avatar);
                realm.commitTransaction();
                intent.putExtra("id",id);
                startActivity(intent);
                break;

            case R.id.btn_suporte:
                avatar.setNome("suporte");
                avatar.setCategorias("suporte");
                jogador.setAvatar(avatar);
                realm.commitTransaction();
                intent.putExtra("id",id);
                startActivity(intent);
                break;

            case R.id.btn_asiatico:
                avatar.setNome("asiatico");
                avatar.setCategorias("asiatico");
                jogador.setAvatar(avatar);
                realm.commitTransaction();
                intent.putExtra("id",id);
                startActivity(intent);
                break;
        }

    }
}
