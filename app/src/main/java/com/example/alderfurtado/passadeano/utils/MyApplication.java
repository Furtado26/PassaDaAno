package com.example.alderfurtado.passadeano.utils;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by alder.furtado on 29/05/2018.
 */

public class MyApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        Realm.deleteRealm(configuration);
        Realm.setDefaultConfiguration(configuration);

    }
}
