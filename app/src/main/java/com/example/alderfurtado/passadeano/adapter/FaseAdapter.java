package com.example.alderfurtado.passadeano.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Jogador;
import com.example.alderfurtado.passadeano.models.Jogo;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by alder.furtado on 04/06/2018.
 */

public class FaseAdapter extends RecyclerView.Adapter<FaseAdapter.FaseHolder> {

    Context ctx;
    RealmList<Jogo> jogos;
    String id_jogador;
    int certa;
    ArrayList<String> respostas = new ArrayList<>();

    Realm realm = Realm.getDefaultInstance();
    public  FaseAdapter(Context ctx, RealmList<Jogo> jogos,String id_jogador){
        this.ctx = ctx;
        this.jogos = jogos;
        this.id_jogador = id_jogador;

    }
    @Override
    public FaseAdapter.FaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(this.ctx).inflate(R.layout.item_fase,parent,false);
        return new FaseAdapter.FaseHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FaseAdapter.FaseHolder holder, final int position) {

        final Jogador jogador = realm.where(Jogador.class).equalTo("id",id_jogador).findFirst();

        holder.tv_perunta.setText(jogos.get(position).getPergunta());
        holder.btn_r1.setText(jogos.get(position).getRepostas().get(0));
        holder.btn_r2.setText(jogos.get(position).getRepostas().get(1));
        holder.btn_r3.setText(jogos.get(position).getRepostas().get(2));

        holder.btn_r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String resposta = holder.btn_r1.getText().toString();
                if(resposta.equals(jogos.get(position).getCerta())){
                    respostas.add(resposta);

//                    realm.beginTransaction();
//                    jogador.setRanking(jogador.getRanking()+100);
//                    realm.insertOrUpdate(jogador);
//                    realm.commitTransaction();
                    Log.i("usuario",jogador.getRanking()+"");

                }
            }
        });

        holder.btn_r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String resposta = holder.btn_r2.getText().toString();
                if(resposta.equals(jogos.get(position).getCerta())){
                    respostas.add(resposta);

//                    realm.beginTransaction();
//                    jogador.setRanking(jogador.getRanking()+100);
//                    realm.insertOrUpdate(jogador);
//                    realm.commitTransaction();
                    Log.i("usuario",jogador.getRanking()+"");
                }
            }
        });

        holder.btn_r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String resposta = holder.btn_r3.getText().toString();
                if(resposta.equals(jogos.get(position).getCerta())){

                    respostas.add(resposta);
//                    realm.beginTransaction();
//                    jogador.setRanking(jogador.getRanking()+100);
//                    realm.insertOrUpdate(jogador);
//                    realm.commitTransaction();
                    Log.i("usuario",jogador.getRanking()+"");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return jogos.size();
    }


    public ArrayList<String> respostas(){
        return this.respostas;
    }

    public class FaseHolder extends RecyclerView.ViewHolder{

        TextView tv_perunta;
        Button btn_r1;
        Button btn_r2;
        Button btn_r3;


        public FaseHolder(View itemView) {
            super(itemView);

            btn_r1 = itemView.findViewById(R.id.btn_r1);
            btn_r2 = itemView.findViewById(R.id.btn_r2);
            btn_r3 = itemView.findViewById(R.id.btn_r3);


            tv_perunta = itemView.findViewById(R.id.tv_pergunta);
        }

    }
}
