package com.example.alderfurtado.passadeano.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alderfurtado.passadeano.adapter.FaseAdapter;
import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Cenario;
import com.example.alderfurtado.passadeano.models.Jogador;

import io.realm.Realm;

public class FaseActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    FaseAdapter faseAdapter;
    Realm realm = Realm.getDefaultInstance();
    Button btn_salvar;
    int posicao = 1;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fase);


        recyclerView = findViewById(R.id.rv_fase);
        btn_salvar = findViewById(R.id.btn_salvar);
        final int id_fase = getIntent().getIntExtra("id_fase",0);
        String id_jogador = getIntent().getStringExtra("id_jogador");
        final Cenario cenario = realm.where(Cenario.class).equalTo("id",id_fase).findFirst();

        final Jogador jogador = realm.where(Jogador.class).equalTo("id",id_jogador).findFirst();
        faseAdapter = new FaseAdapter(getBaseContext(),cenario.getJogos(),id_jogador);

        linearLayoutManager = new LinearLayoutManager(getBaseContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        btn_salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int acertos = 0;

                if(posicao == 3){
                    Log.i("repostas",faseAdapter.respostas().toString());
                    for(int i =0; i < faseAdapter.respostas().size();i++){
                        faseAdapter.respostas().contains(cenario.getJogos().get(i).getRepostas());
                        acertos = acertos + 1;
                    }

                    realm.beginTransaction();
                    jogador.adicionarRanking(id_fase -1,acertos);
                    realm.commitTransaction();
                    if(acertos <3){
                        Toast.makeText(getBaseContext(),"Você acerto "+acertos+" de 3",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getBaseContext(),"Parabéns! Você passo de nível",Toast.LENGTH_SHORT).show();
                    }

                    Log.i("pontuacao",jogador.getRanking().get(id_fase-1)+"");
                    onBackPressed();
                }else{
                    linearLayoutManager.scrollToPosition(posicao);
                    posicao++;
                }

            }
        });
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(faseAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
