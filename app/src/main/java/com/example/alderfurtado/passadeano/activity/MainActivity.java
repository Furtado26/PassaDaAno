package com.example.alderfurtado.passadeano.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Cenario;
import com.example.alderfurtado.passadeano.models.Jogo;

import io.realm.Realm;
import io.realm.RealmList;

public class MainActivity extends AppCompatActivity {

    Button btn_comeca;
    Button btn_ranking;
    Button btn_sobre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.gerarJogo1();
        this.gerarJogo2();
        this.gerarJogo3();

        btn_comeca = findViewById(R.id.btn_comeca);
        btn_ranking = findViewById(R.id.btn_ranking);
        btn_sobre = findViewById(R.id.btn_regras);

        btn_sobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RegrasActivity.class);
                startActivity(intent);
            }
        });

        btn_comeca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,CadastroActivity.class);
                startActivity(intent);
            }
        });

        btn_ranking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RankinActivity.class);
                startActivity(intent);
            }
        });

    }

    void gerarJogo1(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Cenario cenario = realm.createObject(Cenario.class,1);


        Jogo jogo = realm.createObject(Jogo.class);
        Jogo jogo2 = realm.createObject(Jogo.class);
        Jogo jogo3 = realm.createObject(Jogo.class);

        jogo.setPergunta("Com relação às memórias do computador");
        jogo.adicionarRespostas("a memória RAM é uma memória volátil, pois seu conteúdo não permanece após desligado ocomputador");
        jogo.adicionarRespostas("a memória DRAM é mais rápida, que amemória SRAM");
        jogo.adicionarRespostas("a memória REM é um melhoramento damemória ROM, permitindo o acesso maisrápido");
        jogo.setCerta("a memória RAM é uma memória volátil, pois seu conteúdo não permanece após desligado ocomputador");

        jogo2.setPergunta("Um conjunto de variáveis com um mesmo nome,diferenciadas apenas por um índice, é denominado");
        jogo2.adicionarRespostas("Método");
        jogo2.adicionarRespostas("Classe");
        jogo2.adicionarRespostas("Array");
        jogo2.setCerta("Array");

        jogo3.setPergunta("O numero 112 em decimal");
        jogo3.adicionarRespostas("1110000");
        jogo3.adicionarRespostas("1110010");
        jogo3.adicionarRespostas("1110100");
        jogo3.setCerta("1110000");

        RealmList<Jogo> jogos = new RealmList<>();
        jogos.add(jogo);
        jogos.add(jogo2);
        jogos.add(jogo3);


        cenario.setNome("1º Semestre");
        cenario.setDificuldade(1);
        cenario.setJogos(jogos);

        realm.commitTransaction();
    }

    void gerarJogo2(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Cenario cenario = realm.createObject(Cenario.class,2);


        Jogo jogo = realm.createObject(Jogo.class);
        Jogo jogo2 = realm.createObject(Jogo.class);
        Jogo jogo3 = realm.createObject(Jogo.class);

        jogo.setPergunta("Uma das características da programação orientada a objetos está relacionada com a proteção dos atributos internos dos objetos contra modificações diretas. As alterações dos atributos devem ocorrer por meio de métodos adequados, " + "\n"+"" +
                "criados para acesso e modificação desses atributos. Essa característica é conhecida como");
        jogo.adicionarRespostas("encapsulamento");
        jogo.adicionarRespostas("polimorfismo");
        jogo.adicionarRespostas("sobrecarga de operador");
        jogo.setCerta("encapsulamento");

        jogo2.setPergunta("Sobre a arquitetura RISC é correto afirmar o seguinte:");
        jogo2.adicionarRespostas("O uso de pipeline é uma característica da RISC");
        jogo2.adicionarRespostas("Comumente, as instruções RISC consomem vários ciclos de clock");
        jogo2.adicionarRespostas("Em comparação com a CISC, RISC apresenta uma arquitetura com poucos registradores");
        jogo2.setCerta("O uso de pipeline é uma característica da RISC");

        jogo3.setPergunta("O numero 12 em decimal");
        jogo3.adicionarRespostas("1100");
        jogo3.adicionarRespostas("1101");
        jogo3.adicionarRespostas("0100");
        jogo3.setCerta("1100");

        RealmList<Jogo> jogos = new RealmList<>();
        jogos.add(jogo);
        jogos.add(jogo2);
        jogos.add(jogo3);


        cenario.setNome("1º Semestre");
        cenario.setDificuldade(3);
        cenario.setJogos(jogos);

        realm.commitTransaction();
    }

    void gerarJogo3(){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Cenario cenario = realm.createObject(Cenario.class,3);


        Jogo jogo = realm.createObject(Jogo.class);
        Jogo jogo2 = realm.createObject(Jogo.class);
        Jogo jogo3 = realm.createObject(Jogo.class);

        jogo.setPergunta("Qual estrutura de dados é caracterizada por: o primeiro elemento a ser retirado deve ser o último que foi inserido");
        jogo.adicionarRespostas("Fila");
        jogo.adicionarRespostas("Pilha");
        jogo.adicionarRespostas("Lista");
        jogo.setCerta("Fila");

        jogo2.setPergunta("Numero flutuante em C é declarado");
        jogo2.adicionarRespostas("Real");
        jogo2.adicionarRespostas("Int");
        jogo2.adicionarRespostas("Float");
        jogo2.setCerta("Float");

        jogo3.setPergunta("O numero 2 em decimal");
        jogo3.adicionarRespostas("10");
        jogo3.adicionarRespostas("11");
        jogo3.adicionarRespostas("12");
        jogo3.setCerta("10");

        RealmList<Jogo> jogos = new RealmList<>();
        jogos.add(jogo);
        jogos.add(jogo2);
        jogos.add(jogo3);


        cenario.setNome("1º Semestre");
        cenario.setDificuldade(3);
        cenario.setJogos(jogos);

        realm.commitTransaction();
    }
}
