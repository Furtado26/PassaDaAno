package com.example.alderfurtado.passadeano.models;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by alder.furtado on 04/06/2018.
 */

public class Jogo extends RealmObject{

    private String pergunta;

    public RealmList<String> getRepostas() {
        return repostas;
    }

    public void setRepostas(RealmList<String> repostas) {
        this.repostas = repostas;
    }

    private RealmList<String> repostas;
    private String certa;

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }


    public String getCerta() {
        return certa;
    }

    public void setCerta(String certa) {
        this.certa = certa;
    }

    public void adicionarRespostas(String resposta){
        this.repostas.add(resposta);
    }


}
