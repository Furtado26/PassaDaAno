package com.example.alderfurtado.passadeano.models;

import io.realm.RealmList;
import io.realm.RealmObject;

import io.realm.annotations.PrimaryKey;

/**
 * Created by alder.furtado on 29/05/2018.
 */

public class Jogador extends RealmObject{


    @PrimaryKey
    private String id;
    private String nome;
    private int matricula;
    private Avatar avatar;
    private RealmList<Integer> ranking;

    public int getRanking_total() {
        return ranking_total;
    }

    public void setRanking_total() {
        this.ranking_total = ranking.get(0)+ranking.get(1)+ranking.get(2);
    }

    private int ranking_total;

    public RealmList<Integer> getRanking() {
        return ranking;
    }

    public void setRanking(RealmList<Integer> ranking) {
        this.ranking = ranking;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {

        this.id = id;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public void adicionarRanking(int fase,int pontuacao){
        ranking.add(fase,pontuacao);
        try{
            if(ranking.get(2) != null){
                this.setRanking_total();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }



}
