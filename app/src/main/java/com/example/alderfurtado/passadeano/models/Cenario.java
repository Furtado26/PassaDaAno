package com.example.alderfurtado.passadeano.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Cenario extends RealmObject {

    @PrimaryKey
    private int id;
    private String nome;
    private int dificuldade;
    private RealmList<Jogo> jogos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    public RealmList<Jogo> getJogos() {
        return jogos;
    }

    public void setJogos(RealmList<Jogo> jogos) {
        this.jogos = jogos;
    }


}
