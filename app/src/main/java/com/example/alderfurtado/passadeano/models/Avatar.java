package com.example.alderfurtado.passadeano.models;

import java.util.ArrayList;

import io.realm.RealmObject;

/**
 * Created by alder.furtado on 29/05/2018.
 */

public class Avatar extends RealmObject{

    private String nome;
    private String categorias;

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
