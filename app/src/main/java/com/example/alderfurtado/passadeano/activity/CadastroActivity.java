package com.example.alderfurtado.passadeano.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Jogador;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

public class CadastroActivity extends AppCompatActivity {

    private EditText et_nome_jogador;
    private EditText et_matricula_jogador;
    private Button btn_cadastro;

    Realm realm = Realm.getDefaultInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        et_nome_jogador = findViewById(R.id.et_nome_jogador);
        et_matricula_jogador = findViewById(R.id.et_matricula_jogador);
        btn_cadastro = findViewById(R.id.btn_cadastro);

        btn_cadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmResults<Jogador> jogadors = realm.where(Jogador.class).findAll();
                realm.beginTransaction();
                Jogador jogador = realm.createObject(Jogador.class, UUID.randomUUID().toString());
                jogador.setNome(et_nome_jogador.getText().toString());
                jogador.setMatricula(Integer.parseInt(et_matricula_jogador.getText().toString()));
                jogador.adicionarRanking(0,0);
                jogador.adicionarRanking(1,0);
                jogador.adicionarRanking(2,0);

                realm.copyToRealm(jogador);
                realm.commitTransaction();

                Intent intent = new Intent(CadastroActivity.this,EscolhaAvatarActivity.class);
                intent.putExtra("id",jogador.getId());
                startActivity(intent);

                Log.i("Resultados",jogadors.toString());
            }
        });

    }
}
