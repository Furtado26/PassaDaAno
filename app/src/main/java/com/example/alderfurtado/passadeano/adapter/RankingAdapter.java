package com.example.alderfurtado.passadeano.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Jogador;
import io.realm.RealmResults;

/**
 * Created by alder.furtado on 29/05/2018.
 */

public class RankingAdapter  extends RecyclerView.Adapter<RankingAdapter.RankingHolder>{

    private RealmResults<Jogador> jogadors;
    private Context context;

    public RankingAdapter(RealmResults<Jogador> jogadors, Context context){
        this.jogadors = jogadors;
        this.context = context;
    }

    @Override
    public RankingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(this.context).inflate(R.layout.item_raking,parent,false);
        return new RankingHolder(itemView);

    }

    @Override
    public void onBindViewHolder(RankingHolder holder, int position) {
        int pontuacaoTotal = jogadors.get(position).getRanking().get(0) + jogadors.get(position).getRanking().get(1) + jogadors.get(position).getRanking().get(2);

        holder.tv_nome.setText(jogadors.get(position).getNome());
        holder.tv_avatar.setText(jogadors.get(position).getAvatar().getNome()+" - "+jogadors.get(position).getAvatar().getCategorias());
        holder.tv_ranking.setText(pontuacaoTotal+"");


    }

    @Override
    public int getItemCount() {
        return jogadors.size();
    }

    public class RankingHolder extends RecyclerView.ViewHolder{

        TextView tv_nome;
        TextView tv_avatar;
        TextView tv_ranking;

        public RankingHolder(View itemView) {
            super(itemView);
            tv_nome = itemView.findViewById(R.id.tv_nome);
            tv_avatar = itemView.findViewById(R.id.tv_avatar);
            tv_ranking = itemView.findViewById(R.id.tv_rankin);
        }
    }

}
