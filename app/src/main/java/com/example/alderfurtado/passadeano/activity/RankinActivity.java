package com.example.alderfurtado.passadeano.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.adapter.RankingAdapter;
import com.example.alderfurtado.passadeano.models.Jogador;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


public class RankinActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Realm realm = Realm.getDefaultInstance();
    RankingAdapter rankingAdapter;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rankin);

        recyclerView = findViewById(R.id.ranking);

        RealmResults<Jogador> jogadors = realm.where(Jogador.class).findAll().sort("ranking_total", Sort.DESCENDING);

        rankingAdapter = new RankingAdapter(jogadors,getBaseContext());

        linearLayoutManager = new LinearLayoutManager(getBaseContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(rankingAdapter);

        Log.i("jogadores",jogadors.toString());

    }
}
