package com.example.alderfurtado.passadeano.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.alderfurtado.passadeano.R;
import com.example.alderfurtado.passadeano.models.Jogador;

import io.realm.Realm;

public class NivelamentoActivity extends AppCompatActivity {
    Button btn_fase1;
    Button btn_fase2;
    Button btn_fase3;


    Realm realm = Realm.getDefaultInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivelamento);
        final Intent intent = new Intent(NivelamentoActivity.this,FaseActivity.class);
        final String id = getIntent().getStringExtra("id");

        final Jogador jogador = realm.where(Jogador.class).equalTo("id",id).findFirst();
        btn_fase1 = findViewById(R.id.btn_fase1);
        btn_fase2 = findViewById(R.id.btn_fase2);
        btn_fase3 = findViewById(R.id.btn_fase3);


        btn_fase1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("id_jogador",id);
                intent.putExtra("id_fase",1);
                startActivity(intent);
            }
        });

        btn_fase2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jogador.getRanking().get(0) < 3){
                    Toast.makeText(getBaseContext(),"Você precisa completar o nivel 1",Toast.LENGTH_SHORT).show();
                }else{
                    intent.putExtra("id_jogador",id);
                    intent.putExtra("id_fase",2);
                    startActivity(intent);
                }
            }
        });

        btn_fase3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jogador.getRanking().get(1) < 3){
                    Toast.makeText(getBaseContext(),"Você precisa completar o nivel 2",Toast.LENGTH_SHORT).show();
                }else{
                    intent.putExtra("id_jogador",id);
                    intent.putExtra("id_fase",3);
                    startActivity(intent);
                }
            }
        });

    }
}
